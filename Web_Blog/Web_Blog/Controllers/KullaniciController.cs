﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Web_Blog.Models; 

namespace Web_Blog.Controllers
{
    public class KullaniciController : Controller
    {
        blog_Db db = new blog_Db();
        // GET: Kullanici
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Uye uye)
        {
            var login = db.Uyes.Where(u => u.Kullanici_Adi == uye.Kullanici_Adi).SingleOrDefault();
                if(login.Kullanici_Adi==uye.Kullanici_Adi &&login.Email==uye.Email&&login.Sifre==uye.Sifre)
            {
                Session["Uye_Id"] = login.Uye_Id;
                Session["Kullanici_Adi"] = login.Kullanici_Adi;
                Session["Yetki_id"] = login.Yetki_Id;
                return RedirectToAction("Hakkimizda", "Home");
            }
                else
            {
                return View("Index","Home");
            }

        }

        public ActionResult Logout()
        {
            return RedirectToAction("Index","Home");
            Session["Uye_Id"] = null;
            Session.Abandon();//Sonlandırıyoruz 
        }


        public ActionResult Yeni_Kullanici()
        {
            return View();
        }

       [HttpPost]
        public ActionResult Yeni_Kullanici(Uye uye ,HttpPostedFileBase Foto)
        {   
            if (ModelState.IsValid)
            {
                if (Foto != null)
                {
                    WebImage img = new WebImage(Foto.InputStream);
                    FileInfo Fotoinfo = new FileInfo(Foto.FileName);
                    string newfoto = Guid.NewGuid().ToString() + Fotoinfo.Extension;
                    img.Resize(150, 150);
                    img.Save("~/Uploads/KullaniciFoto/" + newfoto);
                    uye.Foto = "/Uploads/KullaniciFoto/" + newfoto;
                    uye.Yetki_Id = 2;
                    db.Uyes.Add(uye);
                    db.SaveChanges();
                    Session["uyeid"] = uye.Uye_Id;
                    Session["Kullanici_Adi"] = uye.Kullanici_Adi;
                    return RedirectToAction("Index","Home");
                }
                else
                {
                    ModelState.AddModelError("Fotograf ", "Fotograf Seciniz");
                    
                }
            }
            return View(uye);
        }
    }
}