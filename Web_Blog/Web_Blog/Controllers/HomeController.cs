﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Blog.Models;


namespace Web_Blog.Controllers
{
    public class HomeController : Controller
    {
        blog_Db db = new blog_Db();
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Hakkimizda()
        {
            return View();
        }
        public ActionResult Iletisim()
        {
            return View();
        }
        public ActionResult Kategori_Partial()
        {
            return View(db.Kategoris.ToList()) ;
        }

    }
    }
